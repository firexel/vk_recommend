import random
import vk
import json
import sys
import time


MAX_USER=30000000
SAMPLES_CNT=10
TRAINSET_SIZE=250


for i in range(TRAINSET_SIZE):
    start_time = time.time()
    user_id = random.randint(1, MAX_USER)
    sys.stderr.write("getting friends graph for user %s\n" % user_id)
    friends_graph, ball = vk.get_social_ball(user_id, 1)
    friends_list = list(friends_graph.keys())
    non_friends_list = list(ball - set(friends_graph.keys()))
    random.shuffle(friends_list)
    random.shuffle(non_friends_list)
    friends_info = vk.get_users_info(friends_list[:SAMPLES_CNT])
    non_friends_info = vk.get_users_info(non_friends_list[:SAMPLES_CNT])
    print(str(user_id) + "\t" + json.dumps(friends_graph) + "\t" + json.dumps(friends_info) + "\t" + json.dumps(non_friends_info))
    end_time = time.time()
    sys.stderr.write("user processed in %.2f seconds" % (end_time - start_time))
