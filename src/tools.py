from collections import defaultdict
from vk import nx_graph
import pandas as pd
import vk

def jaccard(x, y):
    return len(x.intersection(y))/len(x.union(y))

def graph_features(G, center):
    JACCARD_FRIENDS = 10
    center_friends = set(G.neighbors(center))
    friend_jaccard = {}
    for friend in center_friends:
        friend_jaccard[friend] =jaccard(set(G.neighbors(friend)), center_friends)
    result = dict()
    result['nodes'] = defaultdict(lambda:{})
    for node in G.nodes():
        common_friends = list((set(G.neighbors(node)).intersection(center_friends)))
        common_friends.sort(key=lambda x: friend_jaccard[x], reverse=True)
        result['nodes'][node]['common_friends'] = len(common_friends)/len(center_friends)
        cnt = 0
        for friend in common_friends[:JACCARD_FRIENDS]:
            result['nodes'][node]['jaccard_%s' % cnt] = friend_jaccard[friend]
            cnt += 1
        while cnt < JACCARD_FRIENDS:
            result['nodes'][node]['jaccard_%s' % cnt] = 0.0
            cnt += 1
    return result

def recommend_friends(user_id, clf):
    graph_vk, nodes = vk.get_social_ball(user_id, 1)
    graph_vk = nx_graph(graph_vk)
    g_features = graph_features(graph_vk, str(user_id))
    user_friends = set(graph_vk.neighbors(str(user_id)))
    features = []
    nodes = []
    for node in set(graph_vk.nodes()) - user_friends:
        features.append(g_features['nodes'][node])
        nodes.append(node)
    df = pd.DataFrame(features, index=nodes).fillna(0.0)
    prediction = clf.predict_proba(df)
    prediction_dict = {}
    for i in range(len(df.index)):
        prediction_dict[str(df.index[i])] = prediction[i][1]
    del (prediction_dict[str(user_id)])
    result = [(user, prediction_dict[user]) for user in sorted(prediction_dict.keys(), key=lambda x: -prediction_dict[x])]
    return result
