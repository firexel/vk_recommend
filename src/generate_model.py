import sys
import numpy as np
import pandas as pd
import networkx as nx
import json
from collections import defaultdict
import random
from sklearn.cross_validation import train_test_split
from matplotlib import pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score
import sklearn.ensemble as ensemble
import copy
import pickle
from vk import nx_graph
from tools import graph_features


def parse_line(line):
    user_id, graph_str, friends_info, non_friends_info = line.rstrip("\n").split("\t")
    graph_json = json.loads(graph_str)
    friends_graph = nx_graph(graph_json)
    return user_id, friends_graph, json.loads(friends_info), json.loads(non_friends_info)

def get_dataset():
    cnt = 0
    result = []
    x = 0
    for line in open('training_set'):
        x += 1
        user, graph, friends_info, non_friends_info = parse_line(line)
        if (friends_info == {}) or (non_friends_info == {}):
            continue
        cnt += 1
        if(random.randint(0, 1)):
            potential_friend = random.choice(list(friends_info.keys()))
            features = graph_features(graph,user)['nodes'][potential_friend]
            features['target'] = 1
        else:
            potential_friend = random.choice(list(non_friends_info.keys()))
            features = graph_features(graph,user)['nodes'][potential_friend]
            features['target'] = 0
        result.append(features)
    return pd.DataFrame(result)

data = get_dataset()

target = data['target']
del(data['target'])

train_data, test_data, train_label, test_label = train_test_split(data, target)

clf = ensemble.GradientBoostingClassifier(n_estimators=100)
clf.fit(train_data, train_label)

clf_file = open('classifier.pickle', 'wb')
pickle.dump(clf, clf_file)


