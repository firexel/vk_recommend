from flask import Flask, render_template
from flask import request
import pickle
import vk
from tools import recommend_friends

app = Flask(__name__)
model = pickle.load(open('classifier.pickle', 'rb'))

@app.route("/")
def index():
    N_FRIENDS = 100
    vk_id = request.args.get('vk_id')
    items = []
    if vk_id:
        friend_prediction = recommend_friends(vk_id, model)
        predicted_friends = []
        for user, score in friend_prediction[:N_FRIENDS]:
            predicted_friends.append(user)
        friends_info = vk.get_users_info(predicted_friends)
        for user, score in friend_prediction[:N_FRIENDS]:
            items.append({"id": user,
                          "url": "http://vk.com/id%s" % user,
                          "name": friends_info[user]['first_name'] + " " + friends_info[user]['last_name'],
                          "score": score,
                          "photo": friends_info[user]['photo_100']})
    return render_template('index.html', vk_id=vk_id, items=items)

if __name__ == "__main__":
    app.debug=True
    app.run(port=8000, host='0.0.0.0')
